/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author Chhatrasal
 */
public class HomeController implements Initializable {

    @FXML
    private AnchorPane contentPane;
    @FXML
    private ImageView buttonIssueBook;
    @FXML
    private ImageView buttonReturnBook;
    @FXML
    private ImageView buttonAddBook;
    @FXML
    private ImageView buttonUpdateBook;
    @FXML
    private ImageView buttonAddProfile;
    @FXML
    private ImageView buttonUpdateProfile;
    @FXML
    private ImageView buttonHome;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        addEventHandler(MouseEvent.ANY, (MouseEvent event) -> {
//            System.out.println("Image button clicked");
//            event.consume();
//        });
            
//            try{
//                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource(ISSUE_LOC));
//                Pane newPane = (AnchorPane) fXMLLoader.load();
//                if (newPane != null) {
//                    contentPane.getChildren().clear();
//                    contentPane.getChildren().add(newPane);
//                }
//            }catch(IOException e){
//                System.out.println("" + e.getMessage());
//            }
//        });
//        buttonIssueBook.setOnMouseClicked(value -> {
//            try{
//                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource(ISSUE_LOC));
//                Pane newPane = (AnchorPane) fXMLLoader.load();
//                if (newPane != null) {
//                    contentPane.getChildren().clear();
//                    contentPane.getChildren().add(newPane);
//                }
//            }catch(IOException e){
//                System.out.println("" + e.getMessage());
//            }
//        });
    }

}
