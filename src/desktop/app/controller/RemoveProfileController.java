/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.controller;

import com.google.gson.Gson;
import desktop.app.model.Profile;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import desktop.app.model.CustomConstants;
import org.json.JSONObject;
import webServices.ServerConnection;

/**
 * FXML Controller class
 *
 * @author Chhatrasal
 */
public class RemoveProfileController implements Initializable {

    @FXML
    private CheckBox studentCheckBox;
    @FXML
    private CheckBox facultyCheckBox;
    @FXML
    private Button removeProfileButton;
    @FXML
    private TextField batchTextField;

    private Profile profile;
    @FXML
    private TextField uidTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField branchTextField;
    @FXML
    private Button clearButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        studentCheckBox.setSelected(true);
        setFieldProperties();
        setButtonClickEvents();
    }

    private void setFieldProperties() {
        uidTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                JSONObject jSONObject = new JSONObject(ServerConnection.getMethod(CustomConstants.URL + "profile?userName="
                        + uidTextField.getText() + "&loginType="
                        + (studentCheckBox.isSelected() ? "student" : (facultyCheckBox.isSelected() ? "faculty" : null))));
                if (jSONObject.getInt("code") == 200) {
                    Gson gson = new Gson();
                    profile = gson.fromJson(jSONObject.get("data").toString(), Profile.class);
                    nameTextField.setText(profile.getName());
                    branchTextField.setText(profile.getBranch());
                    batchTextField.setText("" + profile.getBatch());
                }
            } catch (Exception e) {
                System.out.println("" + e.getMessage());
            }
        });
        studentCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (studentCheckBox.isSelected()) {
                facultyCheckBox.setSelected(false);
                batchTextField.setDisable(false);
            }
        });

        facultyCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (facultyCheckBox.isSelected()) {
                studentCheckBox.setSelected(false);
                batchTextField.setDisable(true);
            }
        });
    }

    private void setButtonClickEvents() {
        removeProfileButton.setOnAction(actionEvent -> {
            try {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put(CustomConstants.KEY_UID, Long.valueOf(uidTextField.getText()));
                hashMap.put(CustomConstants.KEY_LOGIN_TYPE, "student");
                System.out.println("Request Data : " + new JSONObject(hashMap).toString());
                JSONObject jsonObject = new JSONObject(ServerConnection.postMethod(CustomConstants.URL + "removeProfile", new JSONObject(hashMap).toString()));
                if (jsonObject.getInt(CustomConstants.RESPONSE_KEY_CODE) == 200) {
                    clearFields();
                } else if (jsonObject.getInt(CustomConstants.RESPONSE_KEY_CODE) == 500) {
                    System.out.println("" + jsonObject.get(CustomConstants.RESPONSE_KEY_DATA).toString());
                }
            } catch (Exception e) {
                System.out.println("API Error : " + e.getMessage());
            }
        });
        clearButton.setOnAction(actionEvet -> clearFields());
    }

    private void clearFields() {
        uidTextField.clear();
        nameTextField.clear();
        batchTextField.clear();
        branchTextField.clear();
        studentCheckBox.setSelected(true);
    }

}
