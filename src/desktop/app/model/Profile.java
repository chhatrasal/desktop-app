/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.model;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Chhatrasal
 */
public class Profile {

    @SerializedName("_id")
    private long uid;
    @SerializedName("name")
    private String name;
    @SerializedName("branch")
    private String branch;
    @SerializedName("batch")
    private int batch;
    @SerializedName("contactNo")
    private long contactNo;
    @SerializedName("password")
    private String password;

    public Profile() {
    }

    public Profile(long uid, String name, String branch, int batch, long contactNo, String password) {
        this.uid = uid;
        this.name = name;
        this.branch = branch;
        this.batch = batch;
        this.contactNo = contactNo;
        this.password = password;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public int getBatch() {
        return batch;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    public long getContactNo() {
        return contactNo;
    }

    public void setContactNo(long contactNo) {
        this.contactNo = contactNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
