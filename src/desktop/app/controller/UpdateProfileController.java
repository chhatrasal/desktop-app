/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.controller;

import com.google.gson.Gson;
import desktop.app.model.Profile;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import desktop.app.model.CustomConstants;
import org.json.JSONException;
import org.json.JSONObject;
import webServices.ServerConnection;

/**
 * FXML Controller class
 *
 * @author Chhatrasal
 */
public class UpdateProfileController implements Initializable {

    @FXML
    private TextField uidTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField contactTextField;
    @FXML
    private TextField batchTextField;
    @FXML
    private TextField branchTextField;
    @FXML
    private Button submitButton;
    @FXML
    private Button clearButton;
    @FXML
    private AnchorPane contentPane;
    @FXML
    private CheckBox studentCheckBox;
    @FXML
    private CheckBox facultyCheckBox;    
    @FXML
    private TextField passwordTextField;

    private Profile profile;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        studentCheckBox.setSelected(true);
        setTextFieldProperties();
        setButtonActionEvent(clearButton, submitButton);
        setFieldProperties();
    }

    private void setButtonActionEvent(Button button1, Button button2) {
        button1.setOnAction(actionEvent -> clearAllFields());
        button2.setOnAction(actionEvent -> {
            try {
                Map<String, Object> hashMap = new HashMap<>();
                if (studentCheckBox.isSelected()) {
                    hashMap.put(CustomConstants.KEY_LOGIN_TYPE, "student");
                    hashMap.put(CustomConstants.KEY_BATCH, Long.valueOf(batchTextField.getText()));
                } else if (facultyCheckBox.isSelected()) {
                    hashMap.put(CustomConstants.KEY_LOGIN_TYPE, "faculty");
                }
                hashMap.put(CustomConstants.KEY_ID, Long.valueOf(uidTextField.getText()));
                hashMap.put(CustomConstants.KEY_NAME, nameTextField.getText());
                hashMap.put(CustomConstants.KEY_BRANCH, branchTextField.getText());
                hashMap.put(CustomConstants.KEY_CONTACT_NO, Long.valueOf(contactTextField.getText()));
                hashMap.put(CustomConstants.KEY_PASSWORD, passwordTextField.getText());

                System.out.println("Requested Data" + new JSONObject(hashMap).toString());
                JSONObject jsonObject = new JSONObject(ServerConnection.postMethod(CustomConstants.URL + "updateProfile", new JSONObject(hashMap).toString()));
                if (jsonObject.getInt(CustomConstants.RESPONSE_KEY_CODE) == 200) {
                    clearAllFields();
                } else if (jsonObject.getInt(CustomConstants.RESPONSE_KEY_CODE) == 500) {
                    System.out.println("" + jsonObject.get(CustomConstants.RESPONSE_KEY_DATA).toString());
                }
            } catch (IOException | NumberFormatException | JSONException e) {
                System.out.println("" + e.getMessage());
            }
        });
    }

    private void clearAllFields() {
        uidTextField.clear();
        nameTextField.clear();
        contactTextField.clear();
        batchTextField.clear();
        branchTextField.clear();
        passwordTextField.clear();
        studentCheckBox.setSelected(true);
        facultyCheckBox.setSelected(false);
    }

    private void setFieldProperties() {
        facultyCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (facultyCheckBox.isSelected()) {
                batchTextField.clear();
                batchTextField.setDisable(true);
                studentCheckBox.setSelected(false);
            } else {
                batchTextField.setDisable(false);
            }
        });

        studentCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (studentCheckBox.isSelected()) {
                batchTextField.setDisable(false);
                facultyCheckBox.setSelected(false);
            }
        });
    }

    private void setTextFieldProperties() {
        uidTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                JSONObject jSONObject = new JSONObject(ServerConnection.getMethod(CustomConstants.URL + "profile?userName="
                        + uidTextField.getText() + "&loginType="
                        + (studentCheckBox.isSelected() ? "student" : (facultyCheckBox.isSelected() ? "faculty" : null))));
                if (jSONObject.getInt("code") == 200) {
                    Gson gson = new Gson();
                    profile = gson.fromJson(jSONObject.get("data").toString(), Profile.class);
                    nameTextField.setText(profile.getName());
                    branchTextField.setText(profile.getBranch());
                    batchTextField.setText("" + profile.getBatch());
                    contactTextField.setText("" + profile.getContactNo());
                    passwordTextField.setText("" + profile.getPassword());
                }
            } catch (Exception e) {
                System.out.println("" + e.getMessage());
            }
        });
    }
}
