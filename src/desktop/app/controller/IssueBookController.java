/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.controller;

import com.google.gson.Gson;
import desktop.app.model.IssueBook;
import desktop.app.model.IssueBookList;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import desktop.app.model.CustomConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import webServices.ServerConnection;

/**
 * FXML Controller class
 *
 * @author Chhatrasal
 */
public class IssueBookController implements Initializable {

    @FXML
    private TextField uidTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField branchTextField;
    @FXML
    private TextField batchTextField;
    @FXML
    private TableView<IssueBookList> issueTableList;
    @FXML
    private TableColumn<IssueBookList, Long> bookIdColumn;
    @FXML
    private TableColumn<IssueBookList, String> bookNameColumn;
    @FXML
    private TableColumn<IssueBookList, String> authorColumn;
    @FXML
    private TableColumn<IssueBookList, Long> issueDateColumn;
    @FXML
    private TableColumn<IssueBookList, Boolean> selectionColumn;
    @FXML
    private Button issueButton;
    @FXML
    private Button clearButton;
    @FXML
    private AnchorPane contentPane;
    @FXML
    private CheckBox studentCheckBox;
    @FXML
    private CheckBox facultyCheckBox;

    private IssueBook issuBookData;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        studentCheckBox.setSelected(true);
        setTextFieldListeners(uidTextField);
        setUpIssueBookTable();
        setButtonActionEvent();
        setFieldProperties();
    }

    private void setTextFieldListeners(TextField uidTextField) {
        uidTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                if (studentCheckBox.isSelected() || facultyCheckBox.isSelected()) {
                    JSONObject jSONObject = new JSONObject(ServerConnection.getMethod(CustomConstants.URL + "cartList?id="
                            + uidTextField.getText() + "&purpose=issueCart&loginType=" + (studentCheckBox.isSelected() ? "student" : "faculty")));
                    if (jSONObject.getInt(CustomConstants.RESPONSE_KEY_CODE) == 200) {
                        JSONArray outputArray = jSONObject.getJSONArray("data");
                        Gson gson = new Gson();
//                        issuBookData = gson.fromJson(jSONObject.get("data").toString(), IssueBook.class);
                        nameTextField.setText(issuBookData.getName());
                        branchTextField.setText(issuBookData.getBranch());
                        batchTextField.setText(String.valueOf(issuBookData.getBatch()));
                        issueTableList.setItems(FXCollections.observableArrayList(issuBookData.getIssuedBooks()));
                        if (issuBookData.getLoginType().toLowerCase().equalsIgnoreCase("student")) {
                            studentCheckBox.setSelected(true);
                        } else if (issuBookData.getLoginType().toLowerCase().equalsIgnoreCase("faculty")) {
                            facultyCheckBox.setSelected(true);
                        }
                    } else if (jSONObject.getInt(CustomConstants.RESPONSE_KEY_CODE) == 500) {
                                System.out.println("API Error : " + jSONObject.get(CustomConstants.RESPONSE_KEY_DATA).toString());
                    }
                }
            } catch (Exception e) {
                   System.out.println("Error : " + e.getMessage());
            }
        });
    }

    private void setUpIssueBookTable() {
        bookIdColumn.setCellValueFactory(new PropertyValueFactory<>("bookId"));
        bookIdColumn.setResizable(true);
        bookNameColumn.setCellValueFactory(new PropertyValueFactory<>("bookName"));
        bookNameColumn.setResizable(true);
        authorColumn.setCellValueFactory(new PropertyValueFactory<>("author"));
        authorColumn.setResizable(true);
        issueDateColumn.setCellValueFactory(new PropertyValueFactory<>("issueDate"));
        issueDateColumn.setCellFactory((TableColumn<IssueBookList, Long> param) -> new TableCell<IssueBookList, Long>() {
            @Override
            protected void updateItem(Long item, boolean empty) {
                try {
                    if (!empty && item != null) {
                        Text text = new Text();
                        text.setText(new SimpleDateFormat("dd-MM-yyyy").format(new Date(issueTableList.getItems().get(getIndex()).getIssueDate())));
                        setGraphic(text);
                    } else {
                        setGraphic(null);
                    }
                } catch (Exception e) {
                    System.out.println("Error : " + e.getMessage());
                }
            }

        });
        issueDateColumn.setResizable(true);
        selectionColumn.setCellValueFactory(new PropertyValueFactory<>("issued"));
        selectionColumn.setCellFactory(new Callback<TableColumn<IssueBookList, Boolean>, TableCell<IssueBookList, Boolean>>() {
            @Override
            public TableCell<IssueBookList, Boolean> call(TableColumn<IssueBookList, Boolean> param) {
                return new TableCell<IssueBookList, Boolean>() {
                    @Override
                    protected void updateItem(Boolean item, boolean empty) {
                        if (!empty && null != item) {
                            CheckBox checkBox = new CheckBox();
                            checkBox.setSelected(item);
                            checkBox.setFocusTraversable(false);
                            checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                                IssueBookList updatItem = issueTableList.getItems().get(getIndex());
                                updatItem.setIssued(checkBox.isSelected());
                                issueTableList.getItems().set(getIndex(), updatItem);
                            });
                            setGraphic(checkBox);
                        } else {
                            setGraphic(null);
                        }
                    }

                };
            }
        });
        selectionColumn.setEditable(true);
        issueTableList.setItems(FXCollections.observableArrayList(new ArrayList<IssueBookList>(5)));
        issueTableList.setEditable(true);
    }

    private void setButtonActionEvent() {
        clearButton.setOnAction(actionEvent -> clearFields());
        issueButton.setOnAction(actionevent -> {
            try {
                ArrayList<JSONObject> outputList = new ArrayList<>();
                for (IssueBookList item : issueTableList.getItems()) {
                    if (item.isIssued()) {
                        Map<String, Object> hashMap = new HashMap<>();
                        hashMap.put(CustomConstants.KEY_UID, uidTextField.getText());
                        hashMap.put(CustomConstants.KEY_LOGIN_TYPE, (studentCheckBox.isSelected() ? "student" : (facultyCheckBox.isSelected() ? "faculty" : "")));
                        hashMap.put(CustomConstants.KEY_ID, item.getBookId());
                        hashMap.put(CustomConstants.KEY_ISBN, item.getIsbn());
                        hashMap.put(CustomConstants.KEY_ISSUED_DATE, item.getIssueDate());
                        outputList.add(new JSONObject(hashMap));
                    }
                }
                JSONObject jsonObject = new JSONObject(ServerConnection.postMethod(CustomConstants.URL + "confirmIssueBooks", new JSONArray(outputList).toString()));
                if (jsonObject.getInt("code") == 200) {
                    uidTextField.clear();
                    clearFields();
                } else if (jsonObject.getInt("code") == 500) {
                    System.out.println("API Error : " + jsonObject.get("data").toString());
                    clearFields();
                }
            } catch (Exception e) {
                System.out.println("Error : " + e.getMessage());
            }
        });
    }

    private void clearFields() {
        uidTextField.clear();
        nameTextField.clear();
        branchTextField.clear();
        batchTextField.clear();
        studentCheckBox.setSelected(true);
        facultyCheckBox.setSelected(false);
        issueTableList.setItems(FXCollections.observableArrayList(new ArrayList<IssueBookList>(5)));
    }

    private void setFieldProperties() {
        facultyCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (facultyCheckBox.isSelected()) {
                batchTextField.clear();
                batchTextField.setDisable(true);
                studentCheckBox.setSelected(false);
            } else {
                batchTextField.setDisable(false);
            }
        });

        studentCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (studentCheckBox.isSelected()) {
                batchTextField.setDisable(false);
                facultyCheckBox.setSelected(false);
            }
        });
    }
}
