/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import org.json.JSONObject;
import webServices.ServerConnection;

/**
 * FXML Controller class
 *
 * @author Chhatrasal
 */
public class UpdateBookController implements Initializable {

    @FXML
    private TextField bookNameTextField;
    @FXML
    private TextField authorTextField;
    private TextField isbnTextField;
    @FXML
    private TextField quantityTextField;
    @FXML
    private Button submitButton;
    @FXML
    private Button clearButton;
    @FXML
    private AnchorPane contentPane;
    @FXML
    private TextField isbnTextField1;
    @FXML
    private TextField startIdTextField;
    @FXML
    private TextField endIdTextField;
    @FXML
    private TextField bookId;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setTextFieldProperties(isbnTextField);
    }

    private void setTextFieldProperties(TextField textField1) {
        textField1.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                JSONObject jSONObject = new JSONObject(ServerConnection.getMethod("http:localhost:4000/app/desktop/book"));
            } catch (Exception e) {
            }
        });
    }

}
