/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 *
 * @author Chhatrasal
 */
public class IssueBook {
    @SerializedName("uid")
    private long uid;
    @SerializedName("name")
    private String name;
    @SerializedName("batch")
    private int batch;
    @SerializedName("branch")
    private String branch;
    @SerializedName("loginType")
    private String loginType;
    @SerializedName("issuedBooks")
    private ArrayList<IssueBookList> issuedBooks;

    public IssueBook() {
    }

    public IssueBook(long uid, String name, int batch, String branch,String loginType, ArrayList<IssueBookList> issuedBooks) {
        this.uid = uid;
        this.name = name;
        this.batch = batch;
        this.branch = branch;
        this.loginType = loginType;
        this.issuedBooks = issuedBooks;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBatch() {
        return batch;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public ArrayList<IssueBookList> getIssuedBooks() {
        return issuedBooks;
    }

    public void setIssuedBooks(ArrayList<IssueBookList> issuedBooks) {
        this.issuedBooks = issuedBooks;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

}
