/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import static desktop.app.model.CustomConstants.ADD_BOOK_LOC;
import static desktop.app.model.CustomConstants.ADD_BOOK_TEXT;
import static desktop.app.model.CustomConstants.ADD_PROFILE_LOC;
import static desktop.app.model.CustomConstants.ADD_PROFILE_TEXT;
import static desktop.app.model.CustomConstants.HOME_LOC;
import static desktop.app.model.CustomConstants.HOME_TEXT;
import static desktop.app.model.CustomConstants.ISSUE_BOOK_TEXT;
import static desktop.app.model.CustomConstants.ISSUE_LOC;
import static desktop.app.model.CustomConstants.REMOVE_PROFILE_LOC;
import static desktop.app.model.CustomConstants.REMOVE_PROFILE_TEXT;
import static desktop.app.model.CustomConstants.RETURN_BOOK_TEXT;
import static desktop.app.model.CustomConstants.RETURN_LOC;
import static desktop.app.model.CustomConstants.UPDATE_BOOK_LOC;
import static desktop.app.model.CustomConstants.UPDATE_BOOK_TEXT;
import static desktop.app.model.CustomConstants.UPDATE_PROFILE_LOC;
import static desktop.app.model.CustomConstants.UPDATE_PROFILE_TEXT;

/**
 * FXML Controller class
 *
 * @author Chhatrasal
 */
public class RootController implements Initializable {

    @FXML
    private Pane contentPane;
    @FXML
    private TreeView<String> sideMenu;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        try {
            System.out.println("In the initialize");
            setupSideMenu();
//        }
    }

    private void setupSideMenu() {
        System.out.println("Setting up side menu");
        TreeItem<String> root = new TreeItem<>();
        root.setExpanded(true);
        TreeItem<String> addBook = new TreeItem<>();
        addBook.setValue(ADD_BOOK_TEXT);
        TreeItem<String> addProfile = new TreeItem<>();
        addProfile.setValue(ADD_PROFILE_TEXT);
        TreeItem<String> home = new TreeItem<>();
        home.setValue(HOME_TEXT);
        TreeItem<String> issueBook = new TreeItem<>();
        issueBook.setValue(ISSUE_BOOK_TEXT);
        TreeItem<String> returnBook = new TreeItem<>();
        returnBook.setValue(RETURN_BOOK_TEXT);
        TreeItem<String> updateBook = new TreeItem<>();
        updateBook.setValue(UPDATE_BOOK_TEXT);
        TreeItem<String> updateProfile = new TreeItem<>();
        updateProfile.setValue(UPDATE_PROFILE_TEXT);
        TreeItem<String> removeProfile = new TreeItem<>();
        removeProfile.setValue(REMOVE_PROFILE_TEXT);

        sideMenu.setRoot(root);
        sideMenu.setShowRoot(false);

        sideMenu.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                System.out.println("old Value " + oldValue);
                System.out.println("new Value " + newValue);
                FXMLLoader fXMLLoader = null;
                Pane newPane = null;
                if (newValue.getValue().equalsIgnoreCase(HOME_TEXT)) {
                    fXMLLoader = new FXMLLoader(getClass().getResource(HOME_LOC));
                    System.out.println("" + getClass().getResource(HOME_LOC));
                } else if (newValue.getValue().equalsIgnoreCase(ISSUE_BOOK_TEXT)) {
                    fXMLLoader = new FXMLLoader(getClass().getResource(ISSUE_LOC));
                } else if (newValue.getValue().equalsIgnoreCase(RETURN_BOOK_TEXT)) {
                    fXMLLoader = new FXMLLoader(getClass().getResource(RETURN_LOC));
                } else if (newValue.getValue().equalsIgnoreCase(ADD_PROFILE_TEXT)) {
                    fXMLLoader = new FXMLLoader(getClass().getResource(ADD_PROFILE_LOC));
                } else if (newValue.getValue().equalsIgnoreCase(UPDATE_PROFILE_TEXT)) {
                    fXMLLoader = new FXMLLoader(getClass().getResource(UPDATE_PROFILE_LOC));
                } else if (newValue.getValue().equalsIgnoreCase(ADD_BOOK_TEXT)) {
                    fXMLLoader = new FXMLLoader(getClass().getResource(ADD_BOOK_LOC));
                } else if (newValue.getValue().equalsIgnoreCase(UPDATE_BOOK_TEXT)) {
                    fXMLLoader = new FXMLLoader(getClass().getResource(UPDATE_BOOK_LOC));
                } else if (newValue.getValue().equalsIgnoreCase(REMOVE_PROFILE_TEXT)){
                    fXMLLoader = new FXMLLoader(getClass().getResource(REMOVE_PROFILE_LOC));
                }
                System.out.println("" + fXMLLoader);
                newPane = (AnchorPane) fXMLLoader.load();
                System.out.println("" + newPane);
                if (newPane != null) {
                    contentPane.getChildren().clear();
                    contentPane.getChildren().add(newPane);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
//        root.getChildren().add(home);
        root.getChildren().add(issueBook);
        root.getChildren().add(returnBook);
        root.getChildren().add(addProfile);
        root.getChildren().add(updateProfile);
        root.getChildren().add(removeProfile);
        root.getChildren().add(addBook);
//        root.getChildren().add(updateBook);
    }   
    
}
