/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.model;

import com.google.gson.annotations.SerializedName;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javafx.beans.property.SimpleBooleanProperty;

/**
 *
 * @author Chhatrasal
 */
public class IssueBookList {

    @SerializedName("bookId")
    private long bookId;
    @SerializedName("author")
    private String author;
    @SerializedName("bookName")
    private String bookName;
    @SerializedName("issueDate")
    private long issueDate;
    @SerializedName("isbn")
    private long isbn;
    private boolean issued;

    public IssueBookList() {
    }

    public IssueBookList(long bookId, String author, String bookName, long issueDate, boolean issued, long isbn) {
        this.bookId = bookId;
        this.author = author;
        this.bookName = bookName;
        this.issueDate = issueDate;
        this.issued = issued;
        this.isbn = isbn;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public long getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(long issueDate) {
        this.issueDate = issueDate;
    }

    public boolean isIssued() {
        return issued;
    }

    public void setIssued(boolean issued) {
        this.issued = issued;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "{ 'bookId' : " + bookId + ", 'bookName' : '" + bookName + "', 'author' : '" + author + "', issueDate' : "
                + issueDate + "', 'checkBox' : " + issued + " }";
    }

}
