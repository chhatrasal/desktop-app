/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.controller;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import desktop.app.model.ISBNBookDetails;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import desktop.app.model.CustomConstants;
import org.json.JSONException;
import org.json.JSONObject;
import webServices.ServerConnection;

/**
 * FXML Controller class
 *
 * @author Chhatrasal
 */
public class AddBookController implements Initializable {

    @FXML
    private TextField bookNameTextField;
    @FXML
    private TextField authorTextField;
    @FXML
    private TextField isbnTextField;
    @FXML
    private TextField quantityTextField;
    @FXML
    private Button submitButton;
    @FXML
    private Button clearButton;
    @FXML
    private AnchorPane contentPane;
    @FXML
    private TextField startIdTextField;
    @FXML
    private TextField endIdTextField;
    @FXML
    private TextField bookIdTextField;
    @FXML
    private CheckBox bTech;
    @FXML
    private CheckBox bSc;
    @FXML
    private CheckBox bArch;
    @FXML
    private CheckBox mba;
    @FXML
    private CheckBox mTech;
    @FXML
    private CheckBox mSc;
    @FXML
    private CheckBox mca;
    @FXML
    private CheckBox sems1;
    @FXML
    private CheckBox sems2;
    @FXML
    private CheckBox sems3;
    @FXML
    private CheckBox sems4;
    @FXML
    private CheckBox sems5;
    @FXML
    private CheckBox sems6;
    @FXML
    private CheckBox sems7;
    @FXML
    private CheckBox sems8;
    @FXML
    private CheckBox sems9;
    @FXML
    private CheckBox sems10;
    @FXML
    private CheckBox cse;
    @FXML
    private CheckBox ece;
    @FXML
    private CheckBox me;
    @FXML
    private CheckBox ee;
    @FXML
    private CheckBox ce;
    @FXML
    private CheckBox arch;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setTextFieldProperties();
        setButtonActionEvent(clearButton, submitButton);
    }

    private void clearAllFields() {
        bookNameTextField.clear();
        authorTextField.clear();
        quantityTextField.clear();
        startIdTextField.clear();
        endIdTextField.clear();
        bookIdTextField.clear();
        bTech.setSelected(false);
        mTech.setSelected(false);
        bSc.setSelected(false);
        mSc.setSelected(false);
        bArch.setSelected(false);
        mca.setSelected(false);
        mba.setSelected(false);
        sems1.setSelected(false);
        sems2.setSelected(false);
        sems3.setSelected(false);
        sems4.setSelected(false);
        sems5.setSelected(false);
        sems6.setSelected(false);
        sems7.setSelected(false);
        sems8.setSelected(false);
        sems9.setSelected(false);
        sems10.setSelected(false);
        cse.setSelected(false);
        ece.setSelected(false);
        ee.setSelected(false);
        me.setSelected(false);
        ce.setSelected(false);
        arch.setSelected(false);
    }

    private void setButtonActionEvent(Button button1, Button button2) {
        button1.setOnAction(actioneEvent -> {
            isbnTextField.clear();
            clearAllFields();
        });
        button2.setOnAction(actionEvent -> {
            try {
                Map<String, Object> hashMap = new HashMap<>();
                hashMap.put(CustomConstants.KEY_ID, Long.valueOf(isbnTextField.getText()));
                hashMap.put(CustomConstants.KEY_BOOK_NAME, bookNameTextField.getText());
                hashMap.put(CustomConstants.KEY_AUTHOR, authorTextField.getText());
                if (!quantityTextField.isDisabled()) {
                    hashMap.put(CustomConstants.KEY_QUANTITY, Integer.valueOf(quantityTextField.getText()));
                }
                if (bookIdTextField.isDisabled()) {
                    hashMap.put(CustomConstants.KEY_START, Integer.valueOf(startIdTextField.getText()));
                    hashMap.put(CustomConstants.KEY_END, Integer.valueOf(endIdTextField.getText()));
                } else {
                    hashMap.put(CustomConstants.KEY_BOOK_ID, Integer.valueOf(bookIdTextField.getText()));
                }
                ArrayList<String> tagList = new ArrayList<>();
                if (bTech.isSelected() && !bTech.isDisable()) {
                    tagList.add("btech".toLowerCase());
                }
                if (bSc.isSelected() && !bSc.isDisable()) {
                    tagList.add("bsc".toLowerCase());
                }
                if (bArch.isSelected() && !bArch.isDisable()) {
                    tagList.add("barch".toLowerCase());
                }
                if (mba.isSelected() && !mba.isDisable()) {
                    tagList.add("mba".toLowerCase());
                }
                if (mTech.isSelected() && !mTech.isDisable()) {
                    tagList.add("mTexh".toLowerCase());
                }
                if (mSc.isSelected() && !mSc.isDisable()) {
                    tagList.add("mSc".toLowerCase());
                }
                if (mca.isSelected() && !mca.isDisable()) {
                    tagList.add("mca".toLowerCase());
                }
                if (sems1.isSelected() && !sems1.isDisable()) {
                    tagList.add("sems 1".toLowerCase());
                }
                if (sems2.isSelected() && !sems2.isDisable()) {
                    tagList.add("sems 2".toLowerCase());
                }
                if (sems3.isSelected() && !sems3.isDisable()) {
                    tagList.add("sems 3".toLowerCase());
                }
                if (sems4.isSelected() && !sems4.isDisable()) {
                    tagList.add("sems 4".toLowerCase());
                }
                if (sems5.isSelected() && !sems5.isDisable()) {
                    tagList.add("sems 5".toLowerCase());
                }
                if (sems6.isSelected() && !sems6.isDisable()) {
                    tagList.add("sems 6".toLowerCase());
                }
                if (sems7.isSelected() && !sems7.isDisable()) {
                    tagList.add("sems 7".toLowerCase());
                }
                if (sems8.isSelected() && !sems8.isDisable()) {
                    tagList.add("sems 8".toLowerCase());
                }
                if (sems9.isSelected() && !sems9.isDisable()) {
                    tagList.add("sems 9".toLowerCase());
                }
                if (sems10.isSelected() && !sems10.isDisable()) {
                    tagList.add("sems 10".toLowerCase());
                }
                if (cse.isSelected() && !cse.isDisable()) {
                    tagList.add("cse".toLowerCase());
                }
                if (ece.isSelected() && !ece.isDisable()) {
                    tagList.add("ece".toLowerCase());
                }
                if (me.isSelected() && !me.isDisable()) {
                    tagList.add("me".toLowerCase());
                }
                if (ee.isSelected() && !ee.isDisable()) {
                    tagList.add("ee".toLowerCase());
                }
                if (ce.isSelected() && !ce.isDisable()) {
                    tagList.add("ce".toLowerCase());
                }
                if (arch.isSelected() && !arch.isDisable()) {
                    tagList.add("arch".toLowerCase());
                }
                hashMap.put(CustomConstants.KEY_TAGS, tagList);

                JSONObject jsonObject = new JSONObject(ServerConnection.postMethod(CustomConstants.URL + "book", new JSONObject(hashMap).toString()));
                if (jsonObject.getInt("code") == 200) {
                    isbnTextField.clear();
                    clearAllFields();
                } else if (jsonObject.getInt("code") == 500) {
                    System.out.println("" + jsonObject.get("data").toString());
                }
            } catch (IOException | NumberFormatException | JSONException e) {
                System.out.println("" + e.getMessage());
            }
        });
    }

    private void setTextFieldProperties() {
        startIdTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!startIdTextField.getText().isEmpty() || !endIdTextField.getText().isEmpty()) {
                bookIdTextField.setDisable(true);
            } else {
                bookIdTextField.setDisable(false);
            }
        });

        endIdTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!startIdTextField.getText().isEmpty() || !endIdTextField.getText().isEmpty()) {
                bookIdTextField.setDisable(true);
            } else {
                bookIdTextField.setDisable(false);
            }
        });

        bookIdTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!bookIdTextField.getText().isEmpty()) {
                startIdTextField.setDisable(true);
                endIdTextField.setDisable(true);
                quantityTextField.setDisable(true);
            } else {
                startIdTextField.setDisable(false);
                endIdTextField.setDisable(false);
                quantityTextField.setDisable(false);
            }
        });

        startIdTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!quantityTextField.getText().isEmpty()) {
                endIdTextField.setText(String.valueOf(Long.valueOf(startIdTextField.getText()) + Long.valueOf(quantityTextField.getText()) - 1));
            }
        });

        isbnTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                JSONObject jsonObject = new JSONObject(ServerConnection.getMethod(CustomConstants.URL
                        + "book?isbn=" + Long.valueOf(isbnTextField.getText())));
                if (jsonObject.getInt(CustomConstants.RESPONSE_KEY_CODE) == 200) {
                    Gson gson = new Gson();
                    ISBNBookDetails bookDetails = gson.fromJson(jsonObject
                            .get(CustomConstants.RESPONSE_KEY_DATA).toString(), ISBNBookDetails.class);
                    bookNameTextField.setText(bookDetails.getBookName());
                    bookNameTextField.setEditable(false);
                    authorTextField.setText(bookDetails.getAuthor());
                    authorTextField.setEditable(false);
                    quantityTextField.setText(String.valueOf(bookDetails.getQuantity()));
                    quantityTextField.setEditable(false);
                    startIdTextField.setText(String.valueOf(bookDetails.getBookIds().get(0)));
                    endIdTextField.setText(String.valueOf(bookDetails.getBookIds().get(bookDetails.getBookIds().size() - 1)));
                    if (bookDetails.getTagList().contains("bTech".toLowerCase())) {
                        bTech.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("mTexh".toLowerCase())) {
                        mTech.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("bSc".toLowerCase())) {
                        bSc.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("mSc".toLowerCase())) {
                        mSc.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("bArch".toLowerCase())) {
                        bArch.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("mca".toLowerCase())) {
                        mca.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("mba".toLowerCase())) {
                        mba.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 1".toLowerCase())) {
                        sems1.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 2".toLowerCase())) {
                        sems2.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 3".toLowerCase())) {
                        sems3.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 4".toLowerCase())) {
                        sems4.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 5".toLowerCase())) {
                        sems5.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 6".toLowerCase())) {
                        sems6.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 7".toLowerCase())) {
                        sems7.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 8".toLowerCase())) {
                        sems8.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 9".toLowerCase())) {
                        sems9.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("sems 10".toLowerCase())) {
                        sems10.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("cse".toLowerCase())) {
                        cse.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("ece".toLowerCase())) {
                        ece.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("ee".toLowerCase())) {
                        ee.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("me".toLowerCase())) {
                        me.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("ce".toLowerCase())) {
                        ce.setSelected(true);
                    }
                    if (bookDetails.getTagList().contains("arch".toLowerCase())) {
                        arch.setSelected(true);
                    }
                } else {
                    clearAllFields();
                    bookNameTextField.setEditable(true);
                    authorTextField.setEditable(true);
                    quantityTextField.setEditable(true);
                }
            } catch (JsonSyntaxException | IOException | NumberFormatException | JSONException e) {
                System.out.println("" + e.getMessage());
            }
        });
    }

}
