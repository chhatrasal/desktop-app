package webServices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import desktop.app.model.CustomConstants;
import org.json.JSONObject;

public class ServerConnection {

    private static final String GET_METHOD = "GET";
    private static final String POST_METHOD = "POST";

    public static String getMethod(final String urlAddress) throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlAddress);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod(GET_METHOD);
        int responseCode = httpConnection.getResponseCode();
        if (getHTTPResponse(urlAddress, result, httpConnection, responseCode)) return result.toString();
        return null;
    }

    public static String postMethod(String urlAddress, String sendData) throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlAddress);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod(POST_METHOD);
        
        // Add data to post url
        httpConnection.setRequestProperty(CustomConstants.URL_KEY_CONTENT_TYPE, CustomConstants.URL_VALUE_CONTENT_TYPE);
        httpConnection.setDoOutput(true);
        OutputStream outputStream = httpConnection.getOutputStream();
        outputStream.write(sendData.getBytes());
        outputStream.flush();
        outputStream.close();     
        
        int responseCode = httpConnection.getResponseCode();
        if (getHTTPResponse(urlAddress, result, httpConnection, responseCode)) return result.toString();
        return null;
    }

    private static boolean getHTTPResponse(String urlAddress, StringBuilder result, HttpURLConnection httpConnection,
                                           int responseCode) throws IOException {
        if (responseCode == HttpURLConnection.HTTP_OK) {
            System.out.println("\nSending 'GET' request to URL : " + urlAddress);
            System.out.println("Response Code : " + responseCode);
            BufferedReader inputStream = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            String line;
            while ((line = inputStream.readLine()) != null) {
                result.append(line);
            }
            System.out.println("In server connections  :" + result.toString());
            return true;
        }
        return false;
    }
}
