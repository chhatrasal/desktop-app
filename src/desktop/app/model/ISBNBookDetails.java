/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.app.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 *
 * @author Chhatrasal
 */
public class ISBNBookDetails {
    @SerializedName("_id")
    private long isbn;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("author")
    private String author;
    @SerializedName("bookName")
    private String bookName;
    @SerializedName("tags")
    private ArrayList<String> tagList;
    @SerializedName("bookIds")
    private ArrayList<Integer> bookIds;

    public ISBNBookDetails() {
    }

    public ISBNBookDetails(long isbn, int quantity, String author, String bookName, ArrayList<String> tagList, ArrayList<Integer> bookIds) {
        this.isbn = isbn;
        this.quantity = quantity;
        this.author = author;
        this.bookName = bookName;
        this.tagList = tagList;
        this.bookIds = bookIds;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public ArrayList<String> getTagList() {
        return tagList;
    }

    public void setTagList(ArrayList<String> tagList) {
        this.tagList = tagList;
    }

    public ArrayList<Integer> getBookIds() {
        return bookIds;
    }

    public void setBookIds(ArrayList<Integer> bookIds) {
        this.bookIds = bookIds;
    }
    
}
