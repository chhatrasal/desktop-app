package desktop.app.model;

public class CustomConstants {

    public static final String URL = "http://localhost:4000/app/desktop/";
//    public static final String URL = "http://localhost:4000/app/desktop/";
    public static final String RESPONSE_KEY_CODE = "code";
    public static final String RESPONSE_KEY_DATA = "data";
    public static final String RESPONSE_KEY_MESSAGE = "message";

    public static final String HOME_TEXT = "Home";
    public static final String ISSUE_BOOK_TEXT = "Issue Book";
    public static final String RETURN_BOOK_TEXT = "Return Book";
    public static final String ADD_PROFILE_TEXT = "Add Profile";
    public static final String UPDATE_PROFILE_TEXT = "Update Profile";
    public static final String ADD_BOOK_TEXT = "Add Book";
    public static final String UPDATE_BOOK_TEXT = "Update Book";
    public static final String REMOVE_PROFILE_TEXT = "Remove Profile";

    public static final String HOME_LOC = "/desktop/app/view/Home.fxml";
    public static final String ISSUE_LOC = "/desktop/app/view/IssueBook.fxml";
    public static final String RETURN_LOC = "/desktop/app/view/ReturnBook.fxml";
    public static final String ADD_PROFILE_LOC = "/desktop/app/view/AddProfile.fxml";
    public static final String UPDATE_PROFILE_LOC = "/desktop/app/view/UpdateProfile.fxml";
    public static final String ADD_BOOK_LOC = "/desktop/app/view/AddBook.fxml";
    public static final String UPDATE_BOOK_LOC = "/desktop/app/view/UpdateBook.fxml";
    public static final String REMOVE_PROFILE_LOC = "/desktop/app/view/RemoveProfile.fxml";

    public static final String URL_KEY_CONTENT_TYPE = "Content-Type";
    public static final String URL_VALUE_CONTENT_TYPE = "application/json";

    public static final String KEY_ID = "_id";
    public static final String KEY_UID = "uid";
    public static final String KEY_NAME = "name";
    public static final String KEY_FATHER_NAME = "fatherName";
    public static final String KEY_BATCH = "batch";
    public static final String KEY_BRANCH = "branch";
    public static final String KEY_CONTACT_NO = "contactNo";
    public static final String KEY_LOGIN_TYPE = "loginType";
    public static final String KEY_PASSWORD = "password";

    public static final String KEY_ISBN = "isbn";
    public static final String KEY_BOOK_NAME = "bookName";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_START = "start";
    public static final String KEY_QUANTITY = "quantity";
    public static final String KEY_END = "end";
    public static final String KEY_BOOK_ID = "bookId";
    public static final String KEY_TAGS = "tags";
    public static final String KEY_ISSUED_DATE = "issuedDate";
    public static final String KEY_RETURN_DATE = "tags";

}
